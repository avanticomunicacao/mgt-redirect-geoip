<?php

namespace Avanti\RedirectByGeoip\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface ZipcodeTrackSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get Zipcode Tracks list.
     * @return ZipcodeTrackInterface[]
     */
    public function getItems();

    /**
     * Set Zipcode Tracks list.
     *
     * @param ZipcodeTrackInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
