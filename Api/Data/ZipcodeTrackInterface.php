<?php

namespace Avanti\RedirectByGeoip\Api\Data;

/**
 * Zipcode Track interface.
 * @api
 */
interface ZipcodeTrackInterface
{
    const ID = 'id';
    const WEBSITE_CODE = 'website_code';
    const ZIPCODE_FROM = 'zipcode_from';
    const ZIPCODE_TO = 'zipcode_to';
    const CREATED_AT = 'created_at';

    /**
     * Get Zipcode Track id.
     *
     * @return int|null
     */
    public function getId();

    /**
     * Set Zipcode Track id.
     *
     * @param int $zipcodeTrackId
     *
     * @return ZipcodeTrackInterface
     */
    public function setId($zipcodeTrackId);

    /**
     * Get website code.
     *
     * @return string|null
     */
    public function getWebsiteCode();

    /**
     * Set website code.
     *
     * @param string $websiteCode
     *
     * @return ZipcodeTrackInterface
     */
    public function setWebsiteCode($websiteCode);

    /**
     * Get Zipcode From.
     *
     * @return string|null
     */
    public function getZipcodeFrom();

    /**
     * Set Zipcode From.
     *
     * @param string $zipcodeFrom
     *
     * @return ZipcodeTrackInterface
     */
    public function setZipcodeFrom($zipcodeFrom);

    /**
     * Get Zipcode To.
     *
     * @return string|null
     */
    public function getZipcodeTo();

    /**
     * Set Zipcode To.
     *
     * @param string $zipcodeTo
     *
     * @return ZipcodeTrackInterface
     */
    public function setZipcodeTo($zipcodeTo);

    /**
     * Get created_at.
     *
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set created_at.
     *
     * @param string $createdAt
     *
     * @return ZipcodeTrackInterface
     */
    public function setCreatedAt($createdAt);
}
