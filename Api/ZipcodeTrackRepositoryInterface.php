<?php

namespace Avanti\RedirectByGeoip\Api;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Api\SearchCriteriaInterface;
use Avanti\RedirectByGeoip\Api\Data\ZipcodeTrackInterface;
use Avanti\RedirectByGeoip\Api\Data\ZipcodeTrackSearchResultsInterface;

interface ZipcodeTrackRepositoryInterface
{
    /**
     * Save ZipcodeTrack.
     *
     * @param ZipcodeTrackInterface $zipcodeTrack
     * @return ZipcodeTrackInterface
     * @throws LocalizedException
     */
    public function save(ZipcodeTrackInterface $zipcodeTrack);

    /**
     * Retrieve ZipcodeTrack.
     *
     * @param string $zipcodeTrackId
     * @return ZipcodeTrackInterface
     * @throws LocalizedException
     */
    public function get($zipcodeTrackId);

    /**
     * Retrieve Zipcode track matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return ZipcodeTrackSearchResultsInterface
     * @throws LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * Delete Zipcode track.
     *
     * @param ZipcodeTrackInterface $mosaic
     * @return bool true on success
     *
     * @throws LocalizedException
     */
    public function delete(ZipcodeTrackInterface $mosaic);

    /**
     * Delete Zipcode track by ID.
     *
     * @param string $zipcodeTrackId
     * @return bool true on success
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function deleteById($zipcodeTrackId);
}
