<?php

declare(strict_types=1);

namespace Avanti\RedirectByGeoip\Api;

/**
 * Interface CookieInterface
 * @api
 */
interface CookieInterface
{
    /**
     * Get form key cookie
     *
     * @param $name
     * @return string
     */
    public function get($name);

    /**
     * Set cookie
     *
     * @param string $value
     * @param string $name
     * @param int $duration
     * @return void
     */
    public function set($value, $name, $duration = 86400);

    /**
     * Delete cookie
     *
     * @param $name
     * @return void
     */
    public function delete($name);
}
