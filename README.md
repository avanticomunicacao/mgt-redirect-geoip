# Módulo para Magento 2 Avanti Redirect By Geoip

    ``penseavanti/module-redirectbygeoip``

## Principais funcionalidades

Este módulo tem a função de identificar a localização do usuário pelo seu IP de navegação. Identificando também 
qual loja atende a sua localidade.

## Instalação

### Modo 1: Arquivo ZIP

 - Descompactar o arquivo ZIP na pasta `app/code/Avanti`
 - Habilitar o módulo executando no terminal `php bin/magento module:enable Avanti_RedirectByGeoip`
 - Aplique atualizações do banco de dados executando `php bin/magento setup:upgrade`
 - Limpe o cache `php bin/magento cache:flush`

### Modo 2: Composer

 - Inclua o repositório do Composer na configuração executando `composer config repositories.mgt-redirectbygeoip git https://bitbucket.org/avanticomunicacao/mgt-redirect-geoip.git`
 - Instale o módulo via Composer `composer require penseavanti/module-redirectbygeoip`
 - Habilitar o módulo executando no terminal `php bin/magento module:enable Avanti_RedirectByGeoip`
 - Aplique atualizações do banco de dados executando `php bin/magento setup:upgrade`\*
 - Limpe o cache `php bin/magento cache:flush`

## Cadastros e Configuração

### Cadastro de Faixas de CEP

Acessando via painel administrativo o caminho Avanti->Faixas de Cep, você pode realizar o cadastro de faixas de cep que atenderão determinada loja.

#### Cadastro manual
Para cadastrar uma faixa de Cep manualmente, vá em **Adicionar nova Faixa de Cep**.
Escolha o Website da loja em questão, digite o Cep Inicial e o Cep Final que a loja escolhida anteriormente irá atender.

#### Cadastro por importação de CSV
Para cadastrar várias faixas de Cep, vá em **Importar CSV**, em Tipo de entidade, escolha **Importar faixas de Cep para Websites**, em comportamento de Importação, escolha "Adicionar/Atualizar",
selecione o arquivo .csv em Arquivo para Importacão. O CSV deverá conter 4 colunas, como o arquivo de exemplo **Exemplo.csv** que está dentro da pasta do módulo.
Após isso poderá clicar em **Verificar dados**, irá aparecer uma mensagem dizendo que o arquivo é válido, juntamente com um botão **Importar**, onde deverá ser clicado.

### Configuração do módulo

As configurações do módulo são acessados em Lojas->Configuração->Avanti->Redirect By Geoip.

#### Ativar identificação de localização do usuário
Habilita ou não o módulo.

#### Ativar modo debug
Ativando este modo, a identificação não vai ser feita com o IP do usuário, e sim com o IP digitado no campo IP para teste.

#### IP para teste
Campo para inserir um IP para testes.

#### Atualizar base de dados automaticamente?
Habilita ou não a Cron que é executada todo dia, à meia noite que atualizar a base de dados.

#### Chave de licença
Campo para inserir a chave de licença do site Maxmind, que tem o domínio desta base de dados.
Obtenha sua chave de licença em `https://www.maxmind.com`.

#### Baixar base de dados ou Atualizar base de dados
Botão para realizar o download da base de dados, seja nova, ou uma versão atualizada.
