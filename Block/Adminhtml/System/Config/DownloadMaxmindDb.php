<?php

namespace Avanti\RedirectByGeoip\Block\Adminhtml\System\Config;

use Avanti\RedirectByGeoip\Model\Downloader\Maxmind;
use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;

class DownloadMaxmindDb extends Field
{
	/**
	 * @var Maxmind
	 */
	private $downloader;

	/**
	 * @param Context $context
	 * @param Maxmind $downloader
	 * @param array $data
	 */
	public function __construct(
		Context $context,
		Maxmind $downloader,
		array $data = []
	) {
		parent::__construct($context, $data);
		$this->downloader = $downloader;
	}

	/**
	 * @param AbstractElement $element
	 * @return string
	 */
	public function render(AbstractElement $element)
	{
		$element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();
		return parent::render($element);
	}

	/**
	 * @param AbstractElement $element
	 * @return string
	 */
	protected function _getElementHtml(AbstractElement $element)
	{
		$note = '';
		$updateDate = $this->downloader->getUpdateDate();
		if ($updateDate) {
			$note = '<div class="note">'
				. __('Last Update: %1', date('j/m/Y', $updateDate))
				. '</div>';
		}

		$button = $this->getLayout()
			->createBlock('Magento\Backend\Block\Widget\Button')
			->setLabel($updateDate ? __('Update Database') : __('Download Database'))
			->setTitle(__('Archive will be downloaded, unpacked, and *.mmdb database will be placed in \'var/avanti/geoip\' folder as \'maxmind.mmdb\' file.\''))
			->setAfterHtml($note)
			->setDataAttribute([
				'mage-init' => [
					'Avanti_RedirectByGeoip/js/download' => [
						'url' => $this->_urlBuilder->getUrl('avanti_redirectbygeoip/maxmind/download'),
						'licenseKeyField' => '#redirectbygeoip_general_maxmind_license_key'
					]
				]
			]);

		return $button->toHtml();
	}
}
