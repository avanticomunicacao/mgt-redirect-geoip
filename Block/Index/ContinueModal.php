<?php

namespace Avanti\RedirectByGeoip\Block\Index;

use Avanti\RedirectByGeoip\Service\GeoIpService;
use Avanti\RedirectByGeoip\Helper\Data;
use Magento\Framework\HTTP\Client\Curl;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

/**
 * Main continue block
 *
 * @api
 * @since 100.0.2
 */
class ContinueModal extends Template
{
	protected $geoIpService;
	protected $curl;
	protected $helper;

	/**
	 * @param Context $context
	 * @param Curl $curl
	 * @param GeoIpService $geoIpService
	 * @param Data $helper
	 * @param array $data
	 */
	public function __construct(
		Context $context,
		Curl $curl,
		GeoIpService $geoIpService,
		Data $helper,
		array $data = []
	) {
		parent::__construct($context, $data);
		$this->_isScopePrivate = true;
		$this->curl = $curl;
		$this->geoIpService = $geoIpService;
		$this->helper = $helper;
	}

	public function getActualZipcode()
	{
		return $_COOKIE['zipcode'] ?? $this->geoIpService->getZipcodeByIpAddress($this->getClientIp());
	}

	public function getClientIp()
	{
		if ($this->helper->isDebugModeEnable()) {
			return $this->helper->getTestIp();
		}
		return $this->geoIpService->getClientIp();
	}

	/**
	 * Returns action url for contact form
	 *
	 * @return string
	 */
	public function getHref()
	{
		$code = 'default';
		$params = array(
			'postcode' => $this->getActualZipcode()
		);
		$urlSearch = $this->getUrl("searchzipcode/index/searchStore", $params);

		$this->curl->post($urlSearch, json_encode($params));
		$result = $this->curl->getBody();
		$data = json_decode($result, true);
		if (isset($data['code'])) {
			$code = $data['code'];
		}
		$url = $this->getUrl("websiteswitcher/website/changeaction");
		$url .= 'website/' . $code;
		return $this->getUrl($url, ['_secure' => true]);
	}
}
