<?php

namespace Avanti\RedirectByGeoip\Block\Index;

use Avanti\RedirectByGeoip\Service\GeoIpService;
use Avanti\RedirectByGeoip\Service\ViaCepService;
use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;
use Magento\Framework\View\Element\Template;
use Avanti\RedirectByGeoip\Helper\Data;

/**
 * Class Index
 * @package Avanti\RedirectByGeoip\Block
 */
class Index extends Template
{
    protected $helper;

    /**
     * @var GeoIpService
     */
    protected $geoIpService;

    /**
     * Remote Address
     *
     * @var RemoteAddress
     */
    protected $remoteAddress;

    protected $viaCepService;
    /**
     * Index constructor.
     *
     * @param Template\Context $context
     * @param Data $helper
     * @param array $data
     * @param GeoIpService $geoIpService
     */
    public function __construct(
        Template\Context $context,
        Data $helper,
        GeoIpService $geoIpService,
        ViaCepService $viaCepService,
        array $data = []
    ) {
        $this->helper = $helper;
        $this->geoIpService = $geoIpService;
        $this->viaCepService = $viaCepService;
        parent::__construct($context, $data);
    }

    public function getActualLocation()
    {
        if (!$this->helper->isUserIdentificationEnable()) {
            return '';
        }

        if ($this->isActualLocationDefined()) {
            return $_COOKIE['city'] . ' - ' . $_COOKIE['state_short'];
        }

        return $this->printActualLocation($this->geoIpService->validateIp($this->getClientIp()));
    }

    public function isActualLocationDefined()
    {
        if (isset($_COOKIE['city'], $_COOKIE['state_short'])) {
            return true;
        }
        return false;
    }

    public function isWebsiteCodeNotDefined()
    {
        if (!isset($_COOKIE['website_code'])) {
            return true;
        }
        return false;
    }

    public function getClientIp()
    {
        if ($this->helper->isDebugModeEnable()) {
            return $this->helper->getTestIp();
        }
        return $this->geoIpService->getClientIp();
    }

    protected function printActualLocation($currentIp)
    {
        if (isset($_COOKIE['zipcode'])) {
            return $this->locationViaCep($currentIp);
        }
        return $this->locationByGeoIp($currentIp);
    }

    public function locationByGeoIp($currentIp)
    {
        return $this->geoIpService->getCityByIpAddress($currentIp) . ' - ' . $this->geoIpService->getStateShortByIpAddress($currentIp);
    }

    public function isEnable()
    {
        if (!$this->helper->isUserIdentificationEnable()) {
            return false;
        }
        return true;
    }

    public function locationViaCep($currentIp)
    {
        $this->viaCepService->setCep($_COOKIE['zipcode']);
        $address = json_decode($this->viaCepService->execute() ,true);
        if (!is_null($address)) {
            if (array_key_exists('erro',$address)) {
                return $this->locationByGeoIp($currentIp);
            }
            if ($this->checkAddress($address)) {
                return  $address['localidade'] . ' - ' . $address['uf'];
            }
        }
        return $this->locationByGeoIp($currentIp);
    }

    public function checkAddress($address)
    {
        if (isset($address['localidade']) && isset($address['uf'])) {
            return true;
        }
        return false;
    }

    protected function getCacheLifetime()
    {
        return 0;
    }
}





