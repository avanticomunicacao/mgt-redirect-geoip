<?php

namespace Avanti\RedirectByGeoip\Cron;

use Avanti\RedirectByGeoip\Helper\Data;
use Avanti\RedirectByGeoip\Model\Downloader\Maxmind;
use Psr\Log\LoggerInterface;

class UpdateDatabase
{
	protected $logger;
	protected $maxmind;
	protected $helper;

	public function __construct(
		LoggerInterface $logger,
		Maxmind $maxmind,
		Data $helper
	) {
		$this->logger = $logger;
		$this->maxmind = $maxmind;
		$this->helper = $helper;
	}

	/**
	 * Write to system.log
	 *
	 * @return void
	 */
	public function execute()
	{
		if (!$this->helper->isAutoUpdateEnable()) {
			return;
		}
		$this->maxmind
			->setLicense($this->helper->getLicense())
			->download();
		$this->logger->info(__('Database updated.'));
	}
}
