<?php

namespace Avanti\RedirectByGeoip\Model\ZipcodeTrack;

use Avanti\RedirectByGeoip\Model\ResourceModel\ZipcodeTrack\Collection;
use Avanti\RedirectByGeoip\Model\ResourceModel\ZipcodeTrack\CollectionFactory;
use Avanti\RedirectByGeoip\Model\ZipcodeTrack;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Ui\DataProvider\Modifier\PoolInterface;
use Magento\Ui\DataProvider\ModifierPoolDataProvider;

/**
 * Class DataProvider
 */
class DataProvider extends ModifierPoolDataProvider
{
    /**
     * @var Collection
     */
    protected $collection;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var array
     */
    protected $loadedData;

    /**
     * Constructor
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $zipcodeTrackCollectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     * @param PoolInterface|null $pool
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $zipcodeTrackCollectionFactory,
        DataPersistorInterface $dataPersistor,
        array $meta = [],
        array $data = [],
        PoolInterface $pool = null
    ) {
        $this->collection = $zipcodeTrackCollectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data, $pool);
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        /** @var ZipcodeTrack $zipcodeTrack */
        foreach ($items as $zipcodeTrack) {
            $this->loadedData[$zipcodeTrack->getId()] = $zipcodeTrack->getData();
        }

        $data = $this->dataPersistor->get('avanti_redirectbygeoip_zipcodetrack');
        if (!empty($data)) {
            $zipcodeTrack = $this->collection->getNewEmptyItem();
            $zipcodeTrack->setData($data);
            $this->loadedData[$zipcodeTrack->getId()] = $zipcodeTrack->getData();
            $this->dataPersistor->clear('avanti_redirectbygeoip_zipcodetrack');
        }

        return $this->loadedData;
    }
}
