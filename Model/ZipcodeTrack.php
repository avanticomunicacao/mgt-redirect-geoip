<?php

namespace Avanti\RedirectByGeoip\Model;

use Avanti\RedirectByGeoip\Api\Data\ZipcodeTrackInterface;
use Avanti\RedirectByGeoip\Api\ZipcodeTrackRepositoryInterface;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Registry;

/**
 * Zipcode Track model
 */
class ZipcodeTrack extends AbstractModel implements ZipcodeTrackInterface
{
    private $searchCriteriaBuilder;
    private $zipcodeTrackRepository;

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'avanti_redirectbygeoip_zipcodetrack';

    /**
     * Construct.
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(ResourceModel\ZipcodeTrack::class);
    }

    public function __construct(
        SearchCriteriaBuilder $searchCriteriaBuilder,
        ZipcodeTrackRepositoryInterface $zipcodeTrackRepository,
        Context $context,
        Registry $registry,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->zipcodeTrackRepository = $zipcodeTrackRepository;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve zipcode track id
     *
     * @return int
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
     * @inheritDoc
     */
    public function getWebsiteCode()
    {
        return $this->getData(self::WEBSITE_CODE);
    }

    /**
     * @inheritDoc
     */
    public function setWebsiteCode($websiteCode)
    {
        return $this->setData(self::WEBSITE_CODE, $websiteCode);
    }

    /**
     * @inheritDoc
     */
    public function getZipcodeFrom()
    {
        return $this->getData(self::ZIPCODE_FROM);
    }

    /**
     * @inheritDoc
     */
    public function setZipcodeFrom($zipcodeFrom)
    {
        return $this->setData(self::ZIPCODE_FROM, $zipcodeFrom);
    }

    /**
     * @inheritDoc
     */
    public function getZipcodeTo()
    {
        return $this->getData(self::ZIPCODE_TO);
    }

    /**
     * @inheritDoc
     */
    public function setZipcodeTo($zipcodeTo)
    {
        return $this->setData(self::ZIPCODE_TO, $zipcodeTo);
    }

    /**
     * @inheritDoc
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * @inheritDoc
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * Get zipcode track from zipcode.
     * @param $zipcode
     * @return mixed
     * @throws LocalizedException
     */
    public function getFromZipcode($zipcode)
    {
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter('zipcode_from',$zipcode,'lteq')
            ->addFilter('zipcode_to',$zipcode,'gteq')
            ->setPageSize(1)
            ->create();
        $items = $this->zipcodeTrackRepository->getList($searchCriteria)->getItems();
        return array_values($items);
    }
}
