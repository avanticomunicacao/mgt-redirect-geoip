<?php

namespace Avanti\RedirectByGeoip\Model\ResourceModel\ZipcodeTrack;

use Avanti\RedirectByGeoip\Model\ResourceModel\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * Event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'avanti_redirectbygeoip_zipcodetrack';

    /**
     * Event object
     *
     * @var string
     */
    protected $_eventObject = 'zipcodeTrack_collection';

    /**
     * Define resource model.
     * @noinspection ReturnTypeCanBeDeclaredInspection
     */
    protected function _construct()
    {
        $this->_init(\Avanti\RedirectByGeoip\Model\ZipcodeTrack::class, \Avanti\RedirectByGeoip\Model\ResourceModel\ZipcodeTrack::class);
    }
}
