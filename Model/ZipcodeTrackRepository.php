<?php

namespace Avanti\RedirectByGeoip\Model;

use Exception;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;
use Avanti\RedirectByGeoip\Api\ZipcodeTrackRepositoryInterface;
use Avanti\RedirectByGeoip\Api\Data;
use Avanti\RedirectByGeoip\Model\ResourceModel\ZipcodeTrack\CollectionFactory as ZipcodeTrackCollectionFactory;
use Avanti\RedirectByGeoip\Model\ResourceModel\ZipcodeTrack as ResourceZipcodeTrack;

class ZipcodeTrackRepository implements ZipcodeTrackRepositoryInterface
{
    private $collectionProcessor;
    private $storeManager;

    protected $dataZipcodeTrackFactory;
    protected $dataObjectHelper;
    protected $dataObjectProcessor;
    protected $extensibleDataObjectConverter;
    protected $extensionAttributesJoinProcessor;
    protected $zipcodeTrackCollectionFactory;
    protected $zipcodeTrackFactory;
    protected $resource;
    protected $searchResultsFactory;

    /**
     * @param ResourceZipcodeTrack $resource
     * @param ZipcodeTrackFactory $zipcodeTrackFactory
     * @param Data\ZipcodeTrackInterfaceFactory $dataZipcodeTrackFactory
     * @param ZipcodeTrackCollectionFactory $zipcodeTrackCollectionFactory
     * @param Data\ZipcodeTrackSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        ResourceZipcodeTrack $resource,
        ZipcodeTrackFactory $zipcodeTrackFactory,
        Data\ZipcodeTrackInterfaceFactory $dataZipcodeTrackFactory,
        ZipcodeTrackCollectionFactory $zipcodeTrackCollectionFactory,
        Data\ZipcodeTrackSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->resource = $resource;
        $this->zipcodeTrackFactory = $zipcodeTrackFactory;
        $this->zipcodeTrackCollectionFactory = $zipcodeTrackCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataZipcodeTrackFactory = $dataZipcodeTrackFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * {@inheritdoc}
     */
    public function save(Data\ZipcodeTrackInterface $zipcodeTrack)
    {
        try {
            $this->resource->save($zipcodeTrack);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the mosaic: %1',
                $exception->getMessage()
            ));
        }

        return $zipcodeTrack;
    }

    /**
     * {@inheritdoc}
     */
    public function get($zipcodeTrackId)
    {
        $zipcodeTrack = $this->zipcodeTrackFactory->create();
        $this->resource->load($zipcodeTrack, $zipcodeTrackId);
        if (!$zipcodeTrack->getId()) {
            throw new NoSuchEntityException(__('Zipcode track with id "%1" does not exist.', $zipcodeTrackId));
        }

        return $zipcodeTrack;
    }

    /**
     * {@inheritdoc}
     */
    public function getList(SearchCriteriaInterface $criteria)
    {
        $collection = $this->zipcodeTrackCollectionFactory->create();
        $this->collectionProcessor->process($criteria, $collection);

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());

        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(Data\ZipcodeTrackInterface $zipcodeTrack)
    {
        try {
            $this->resource->delete($zipcodeTrack);
        } catch (Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the zipcode track: %1',
                $exception->getMessage()
            ));
        }

        return true;
    }

    /**
     * Delete Zipcode Track by given Zipcode Track Identity
     *
     * @param string $zipcodeTrackId
     * @return bool
     * @throws LocalizedException
     */
    public function deleteById($zipcodeTrackId)
    {
        return $this->delete($this->get($zipcodeTrackId));
    }

    /**
     * Get zipcode track from zipcode.
     * @param $zipcode
     * @return mixed
     */
    public function getFromZipcode($zipcode)
    {
        $collection = $this->zipcodeTrackFactory->create()
            ->getCollection()
            ->addFieldToFilter('zipcode_from', array('gteq' => $zipcode))
            ->addFieldToFilter('zipcode_to', array('lteq' => $zipcode));
        return $collection->getData();
    }
}
