<?php

declare(strict_types=1);

namespace Avanti\RedirectByGeoip\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
    const PATH_USER_IDENTIFICATION_ENABLE = 'redirectbygeoip/general/enable';
    const PATH_DEBUG_MODE_ENABLE = 'redirectbygeoip/general/debug_mode';
    const PATH_TESTING_IP = 'redirectbygeoip/general/testing_ip';
    const PATH_AUTO_UPDATE = 'redirectbygeoip/general/auto_update';
    const PATH_LICENSE = 'redirectbygeoip/general/maxmind_license_key';

    /**
     * @return mixed
     */
    public function isUserIdentificationEnable() : bool
    {
        return (bool) $this->scopeConfig->getValue(self::PATH_USER_IDENTIFICATION_ENABLE,ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function isDebugModeEnable() : bool
    {
        return (bool) $this->scopeConfig->getValue(self::PATH_DEBUG_MODE_ENABLE,ScopeInterface::SCOPE_STORE);
    }

	/**
	 * @return mixed
	 */
	public function isAutoUpdateEnable() : bool
	{
		return (bool) $this->scopeConfig->getValue(self::PATH_AUTO_UPDATE,ScopeInterface::SCOPE_STORE);
	}

    /**
     * @return mixed
     */
    public function getTestIp() : string
    {
        return (string) $this->scopeConfig->getValue(self::PATH_TESTING_IP,ScopeInterface::SCOPE_STORE);
    }

	/**
	 * @return mixed
	 */
	public function getLicense() : string
	{
		return (string) $this->scopeConfig->getValue(self::PATH_LICENSE,ScopeInterface::SCOPE_STORE);
	}

    /**
     * @param $zipcode
     * @return bool|null|string|string[]
     */
    public function formatZip($zipcode)
    {
        $new = trim($zipcode);
        $new = preg_replace('/[^0-9\s]/', '', $new);
        if (!preg_match("/^[0-9]{7,8}$/", $new)) {
            return false;
        }

        if (preg_match("/^[0-9]{7}$/", $new)) {
            $new = "0" . $new;
        }
        return $new;
    }
}
