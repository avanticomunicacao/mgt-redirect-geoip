<?php

namespace Avanti\RedirectByGeoip\Service;

use Exception;
use GeoIp2\Database\Reader;
use GeoIp2\Database\ReaderFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;
use Magento\Framework\Module\Dir as ModuleDirectory;

/**
 * Class GeoIpService
 * @package Avanti\RedirectByGeoip\Service
 */
class GeoIpService
{
	const FILENAME = 'maxmind.mmdb';

	/**
	 * @var Reader
	 */
	private $reader;

	/**
	 * @var ModuleDirectory
	 */
	protected $moduleDirectory;

	/**
	 * @var ReaderFactory
	 */
	protected $readerFactory;

	/**
	 * @var RequestInterface
	 */
	protected $request;

	/**
	 * @var RemoteAddress
	 */
	protected $remoteAddress;

	/**
	 * @var DirectoryList
	 */
	protected $directoryList;

	/**
	 * @param ModuleDirectory $moduleDirectory
	 * @param ReaderFactory $readerFactory
	 * @param RemoteAddress $remoteAddress
	 * @param RequestInterface $request
	 * @param DirectoryList $directoryList
	 */
	public function __construct(
		ModuleDirectory $moduleDirectory,
		ReaderFactory $readerFactory,
		RemoteAddress $remoteAddress,
		RequestInterface $request,
		DirectoryList $directoryList
	) {
		$this->moduleDirectory = $moduleDirectory;
		$this->readerFactory = $readerFactory;
		$this->remoteAddress = $remoteAddress;
		$this->request = $request;
		$this->directoryList = $directoryList;
	}

	/**
	 * @return Reader
	 */
	private function getReader()
	{
		if ($this->reader === null) {
			$this->reader = new Reader($this->getFilepath());
		}
		return $this->reader;
	}

	/**
	 * @return string
	 * @throws NotFoundException
	 */
	private function getFilepath()
	{
		$path = BP . '/var/avanti/geoip/' . self::FILENAME;
		if (file_exists($path)) {
			return $path;
		}

		throw new NotFoundException(__('Maxmind database file was not found.'));
	}

	/**
	 * Lookup country from remote address.
	 *
	 * @param $ipAddress
	 * @return null|string
	 */
	public function getCountryByIpAddress($ipAddress)
	{
		try {
			return $this->getReader()->country($ipAddress)->country->isoCode;
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}

	/**
	 * @param $ipAddress
	 * @return string
	 */
	public function getCityByIpAddress($ipAddress)
	{
		try {
			return $this->getReader()->city($ipAddress)->city->name;
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}

	/**
	 * @param $ipAddress
	 * @return string
	 */
	public function getStateByIpAddress($ipAddress)
	{
		try {
			return $this->getReader()->city($ipAddress)->mostSpecificSubdivision->name;
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}

	/**
	 * @param $ipAddress
	 * @return string
	 */
	public function getStateShortByIpAddress($ipAddress)
	{
		try {
			return $this->getReader()->city($ipAddress)->mostSpecificSubdivision->isoCode;
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}

	/**
	 * @param $ipAddress
	 * @return string
	 */
	public function getZipcodeByIpAddress($ipAddress)
	{
		try {
			return $this->formatZipcode($this->getReader()->city($ipAddress)->postal->code);
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}

	/**
	 * @param $zipcode
	 * @return string
	 */
	private function formatZipcode($zipcode)
	{
		return $zipcode . '000';
	}

	/**
	 * Lookup country from remote address.
	 *
	 * @return null|string
	 */
	public function getCountry()
	{
		return $this->getCountryByIpAddress($this->getClientIp());
	}

	/**
	 * Get user ip address
	 *
	 * @return string
	 */
	public function getClientIp()
	{
	    $currentIp = $this->request->getServer('HTTP_X_FORWARDED_FOR') ?? $this->remoteAddress->getRemoteAddress();
		return $this->validateIp($currentIp);
	}

    /**
     * @param $currentIp
     * @return false|mixed|string|string[]
     */
    public function validateIp($currentIp)
    {
        $currentIp = preg_replace('~\x{00a0}~', '', $currentIp);
        $currentIp = explode(',' , $currentIp);
        if (is_array($currentIp)) {
            $currentIp = $currentIp[0];
        }
        return $currentIp;
    }
}
