<?php

namespace Avanti\RedirectByGeoip\Service;

use GuzzleHttp\Client;
use GuzzleHttp\ClientFactory;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\ResponseFactory;
use Magento\Framework\Webapi\Rest\Request;
use Psr\Log\LoggerInterface;

/**
 * Class ViaCepService
 * @package Avanti\RedirectByGeoip\Service
 */
class ViaCepService
{

    const API_REQUEST_URI = 'https://viacep.com.br/ws/';
    const TYPE_JSON = '/json';

    /**
     * @var ClientFactory
     */
    private $_clientFactory;

    /**
     * @var ResponseFactory
     */
    private $_responseFactory;

    /**
     * @var LoggerInterface
     */
    private $_logger;

    /**
     * @var array
     */
    private $params;

    private $cep;

    /**
     * @var string
     */
    private $apiRequestEndpoint;

    /**
     * @var string
     */
    private $apiRequestUri;

    /**
     * ViaCepService constructor.
     * @param ClientFactory $clientFactory
     * @param ResponseFactory $responseFactory
     * @param System $system
     * @param LoggerInterface $_logger
     */
    public function __construct(
        ClientFactory $clientFactory,
        ResponseFactory $responseFactory,
        LoggerInterface $_logger
    ) {
        $this->_clientFactory = $clientFactory;
        $this->_responseFactory = $responseFactory;
        $this->_logger = $_logger;
        $this->params = [];
    }

    public function setCep($cep)
    {
        $this->cep = $cep;
    }

    /**
     * Do API request with provided params
     *
     * @param string $uriEndpoint
     * @param array $params
     * @param string $requestMethod
     *
     * @return Response
     */
    private function doRequest(
        string $uriEndpoint,
        array $params = [],
        string $requestMethod = Request::HTTP_METHOD_GET
    ): Response {
        if (empty($this->apiRequestUri)) {
            $this->apiRequestUri = self::API_REQUEST_URI;
        }
        $client = $this->_clientFactory->create(['config' => [
            'base_uri' => $this->apiRequestUri
        ]]);
        try {
            $response = $client->request(
                $requestMethod,
                $uriEndpoint,
                $params
            );
        } catch (GuzzleException $exception) {
            $response = $this->_responseFactory->create([
                'status' => $exception->getCode(),
                'reason' => $exception->getMessage()
            ]);
        }
        return $response;
    }

    /**
     * Fetch some data from API
     */
    public function execute()
    {
        $response = $this->doRequest($this->apiRequestEndpoint . $this->cep . self::TYPE_JSON,$this->params);
        $status = $response->getStatusCode();
        if ($status != 200)
        {
            $this->_logger->critical('Error in API request');
            return false;
        }
        $responseBody = $response->getBody();
        $responseContent = $responseBody->getContents();
        return $responseContent;
    }
}
