<?php

namespace Avanti\RedirectByGeoip\Controller\Index;

use Avanti\RedirectByGeoip\Helper\Data;
use Avanti\RedirectByGeoip\Plugin\CookieContextPlugin;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;

class ChangeWebsite extends Action
{
    const CHANGE_WEBSITE_URL = 'websiteswitcher/website/changeaction/website/';

    /**
     * @var CookieContextPlugin
     */
    private $cookieContextPlugin;
    /**
     * @var Data
     */
    private $helper;

    public function __construct(
        Context $context,
        CookieContextPlugin $cookieContextPlugin,
        Data $helper
    ) {
        parent::__construct($context);
        $this->cookieContextPlugin = $cookieContextPlugin;
        $this->helper = $helper;
    }

    public function execute()
    {
        $params = $this->getRequest()->getParams();
        if (!$this->getRequest()->getParam('website') && !$this->getRequest()->getParam('postcode')) {
            return false;
        }
        $zipcode = $this->helper->formatZip($this->getRequest()->getParam('postcode'));
        if (!$zipcode) {
            $params = json_decode(utf8_encode(file_get_contents('php://input')));
            $zipcode = $params->postcode;
        }
        $this->cookieContextPlugin->clearLocationData();
        $this->cookieContextPlugin->setZipcode($zipcode);
        $path = self::CHANGE_WEBSITE_URL . $this->getRequest()->getParam('website');
        return $this->resultRedirectFactory->create()->setPath($path);
    }
}
