<?php

namespace Avanti\RedirectByGeoip\Controller\Index;

use Avanti\RedirectByGeoip\Plugin\CookieContextPlugin;
use Avanti\RedirectByGeoip\Service\GeoIpService;
use Avanti\RedirectByGeoip\Service\ViaCepService;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\Action;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\Result\RawFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\UrlFactory;
use Magento\Store\Api\StoreRepositoryInterface;
use Avanti\RedirectByGeoip\Helper\Data;
use Avanti\RedirectByGeoip\Model\ZipcodeTrack;

class CookieManagement extends Action
{

    const CITY = 'city';

    const STATE_SHORT = 'state_short';

    const ZIPCODE = 'zipcode';

    /**
     * @var CookieContextPlugin
     */
    private $cookieContextPlugin;

    /**
     * @var ViaCepService
     */
    private $viaCepService;

    /**
     * @var GeoIpService
     */
    private $geoIpService;

    private $_storeManager;

    private $helper;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        CookieContextPlugin $cookieContextPlugin,
        GeoIpService $geoIpService,
        ViaCepService $viaCepService,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        Data $helper
    ) {
        parent::__construct($context);
        $this->cookieContextPlugin = $cookieContextPlugin;
        $this->geoIpService = $geoIpService;
        $this->viaCepService = $viaCepService;
        $this->_storeManager = $storeManager;
        $this->helper = $helper;
    }

    public function execute()
    {

        try {
            if ($this->checkCookies()) {
                return $this->resultFactory->create(ResultFactory::TYPE_JSON)->setData([
                    'found' => true,
                    'city' => $_COOKIE['city'],
                    'state_short' => $_COOKIE['state_short']
                ]);
            }
            if (array_key_exists(self::ZIPCODE,$_COOKIE)) {
                $currentIp = $this->geoIpService->getClientIp();
                if ($this->helper->isDebugModeEnable()) {
                    $currentIp = $this->geoIpService->validateIp($this->helper->getTestIp());
                }
                $this->cookieContextPlugin->setLocationByViaCep($currentIp);
                if ($this->checkCookies()) {
                    return $this->resultFactory->create(ResultFactory::TYPE_JSON)->setData([
                        'found' => true,
                        'city' => $_COOKIE['city'],
                        'state_short' => $_COOKIE['state_short']
                    ]);
                }
                $this->viaCepService->setCep($_COOKIE[self::ZIPCODE]);
                $address = json_decode($this->viaCepService->execute() ,true);
                if (!is_null($address)) {
                    if (array_key_exists('erro',$address)) {
                        return $this->resultFactory->create(ResultFactory::TYPE_JSON)->setData([
                            'found' => true,
                            'city' => __('Physical Store'),
                            'state_short' => $this->_storeManager->getStore()->getName()
                        ]);
                    }
                    if ($this->cookieContextPlugin->checkAddress($address)) {
                        return $this->resultFactory->create(ResultFactory::TYPE_JSON)->setData([
                            'found' => true,
                            'city' => $address['localidade'],
                            'state_short' => $address['uf']
                        ]);
                    }
                }
            }
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(
                __('An error occurred while processing your form. Please try again later.')
            );
        }
        return $this->resultRedirectFactory->create()->setPath('*/*/');
    }

    public function checkCookies()
    {
        if (array_key_exists(self::CITY,$_COOKIE) && array_key_exists(self::STATE_SHORT,$_COOKIE)) {
            if (array_key_exists(self::ZIPCODE,$_COOKIE)) {
                return false;
            }
            if (!empty($_COOKIE[self::CITY]) && !is_null(empty($_COOKIE[self::CITY])) && !empty($_COOKIE[self::STATE_SHORT]) && !is_null(empty($_COOKIE[self::STATE_SHORT]))) {
                return true;
            }
        }
        return false;
    }
}
