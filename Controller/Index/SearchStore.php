<?php

namespace Avanti\RedirectByGeoip\Controller\Index;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\Action;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\Result\RawFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\UrlFactory;
use Magento\Store\Api\StoreRepositoryInterface;
use Avanti\RedirectByGeoip\Helper\Data;
use Avanti\RedirectByGeoip\Model\ZipcodeTrack;

class SearchStore extends Action
{
    /**
     * @var StoreRepositoryInterface
     */
    private $storeRepository;

    /**
     * @var Context
     */
    private $context;

    /**
     * @var JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var RawFactory
     */
    protected $resultRawFactory;

	/**
	 * @var ZipcodeTrack
	 */
    protected $zipcodeTrack;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * @param Context $context
     * @param JsonFactory $resultJsonFactory
     * @param RawFactory $resultRawFactory
     * @param UrlFactory $urlFactory
     * @param ZipcodeTrack $zipcodeTrack
     * @param StoreRepositoryInterface $storeRepository
     * @param Data $helper
     */
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        RawFactory $resultRawFactory,
        UrlFactory $urlFactory,
        ZipcodeTrack $zipcodeTrack,
        StoreRepositoryInterface $storeRepository,
        Data $helper
    ) {
        parent::__construct($context);
        $this->context = $context;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->resultRawFactory = $resultRawFactory;
        $this->urlModel = $urlFactory->create();
        $this->zipcodeTrack = $zipcodeTrack;
        $this->storeRepository = $storeRepository;
        $this->helper = $helper;
    }

    /**
     * Post user localization
     * @throws LocalizedException
     */
    public function execute()
    {
    	if (!$this->getRequest()->getParam('postcode')) {
    		return false;
		}
		$zipcode = $this->helper->formatZip($this->getRequest()->getParam('postcode'));
		if (!$zipcode) {
			$params = json_decode(utf8_encode(file_get_contents('php://input')));
			$zipcode = $params->postcode;
		}
		$storeData = array_filter($this->zipcodeTrack->getFromZipcode($zipcode));
        if (empty($storeData) || !isset($storeData)) {
            return $this->resultFactory->create(ResultFactory::TYPE_JSON)->setData([
                'found' => false
            ]);
        }
        try {
            $stores = $this->storeRepository->getList();
            if (array_key_exists($storeData[0]['website_code'], $stores)) {
				$data = $stores[$storeData[0]['website_code']];
				return $this->resultFactory
                    ->create(ResultFactory::TYPE_JSON)
                    ->setData([
                        'found' => true,
                        'name' => $data['name'],
                        'code' => $data['code'],
                        'website_id' => $data['website_id'],
                    ]);
            }
            return $this->resultFactory->create(ResultFactory::TYPE_JSON)->setData([
                'found' => false
            ]);
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(
                __('An error occurred while processing your form. Please try again later.')
            );
        }
        return $this->resultRedirectFactory->create()->setPath('*/*/');
    }
}
