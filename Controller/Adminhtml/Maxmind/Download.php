<?php

namespace Avanti\RedirectByGeoip\Controller\Adminhtml\Maxmind;

use Avanti\RedirectByGeoip\Model\Downloader\Maxmind;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\DataObject;

class Download extends Action
{
	const ADMIN_RESOURCE = 'Avanti_RedirectByGeoip::download_database';

	/**
	 * @var Maxmind
	 */
	private $downloader;

	/**
	 * @param Context $context
	 * @param Maxmind $downloader
	 */
	public function __construct(
		Context $context,
		Maxmind $downloader
	) {
		$this->downloader = $downloader;
		parent::__construct($context);
	}

	/**
	 * @return Json
	 */
	public function execute()
	{
		/** @var Json $resultJson */
		$resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
		$response = new DataObject();

		try {
			$this->downloader
				->setLicense($this->getRequest()->getParam('license'))
				->setEdition('GeoLite2-City')
				->download();

			$response->setMessage(__('Database successfully downloaded.'));
		} catch (\Exception $e) {
			$response->setMessage($e->getMessage());
			$response->setError(1);
		}

		return $resultJson->setData($response);
	}
}
