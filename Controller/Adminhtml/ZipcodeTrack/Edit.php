<?php

namespace Avanti\RedirectByGeoip\Controller\Adminhtml\ZipcodeTrack;

use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Avanti\RedirectByGeoip\Controller\Adminhtml\ZipcodeTrack;
use Avanti\RedirectByGeoip\Model\ZipcodeTrackFactory;

class Edit extends ZipcodeTrack implements HttpGetActionInterface
{
    /**
     * @var PageFactory
     */
    private $resultPageFactory;

    /**
     * @var ZipcodeTrackFactory
     */
    private $zipcodeTrackFactory;

    /**
     * @param Context $context
     * @param Registry $registry
     * @param PageFactory $resultPageFactory
     * @param ZipcodeTrackFactory $zipcodeTrackFactory
     */
    public function __construct(
        Context $context,
        Registry $registry,
        PageFactory $resultPageFactory,
        ZipcodeTrackFactory $zipcodeTrackFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->zipcodeTrackFactory = $zipcodeTrackFactory;
        parent::__construct($context, $registry);
    }

    /**
     * Edit action.
     *
     * @return ResultInterface
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $model = $this->zipcodeTrackFactory->create();

        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This Zipcode Track no longer exists.'));
                /** @var Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }

        $this->registry->register('avanti_redirectbygeoip_zipcodetrack', $model);

        /** @var Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $this->initPage($resultPage)->addBreadcrumb(
            $id ? __('Edit Zipcode Track') : __('New Zipcode Track'),
            $id ? __('Edit Zipcode Track') : __('New Zipcode Track')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Zipcode Tracks'));
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? __('Edit Zipcode Track %1', $model->getId()) : __('New Zipcode Track'));
        return $resultPage;
    }
}
