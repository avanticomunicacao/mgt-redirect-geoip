<?php

namespace Avanti\RedirectByGeoip\Controller\Adminhtml\ZipcodeTrack;

use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Registry;
use Avanti\RedirectByGeoip\Api\ZipcodeTrackRepositoryInterface;
use Avanti\RedirectByGeoip\Controller\Adminhtml\ZipcodeTrack;
use Avanti\RedirectByGeoip\Helper\Data;
use Avanti\RedirectByGeoip\Model\ZipcodeTrackFactory;

class Save extends ZipcodeTrack implements HttpPostActionInterface
{
    /**
     * @var DataPersistorInterface
     */
    private $dataPersistor;

    /**
     * @var ZipcodeTrackFactory
     */
    private $zipcodeTrackFactory;

    /**
     * @var ZipcodeTrackRepositoryInterface
     */
    private $zipcodeTrackRepository;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * @param Data $helper
     * @param Context $context
     * @param Registry $registry
     * @param DataPersistorInterface $dataPersistor
     * @param ZipcodeTrackFactory $zipcodeTrackFactory
     * @param ZipcodeTrackRepositoryInterface $zipcodeTrackRepository
     */
    public function __construct(
        Data $helper,
        Context $context,
        Registry $registry,
        DataPersistorInterface $dataPersistor,
        ZipcodeTrackFactory $zipcodeTrackFactory = null,
        ZipcodeTrackRepositoryInterface $zipcodeTrackRepository = null
    ){
        $this->helper = $helper;
        $this->dataPersistor = $dataPersistor;
        $this->zipcodeTrackFactory = $zipcodeTrackFactory
            ?: ObjectManager::getInstance()->get(ZipcodeTrackFactory::class);
        $this->zipcodeTrackRepository = $zipcodeTrackRepository
            ?: ObjectManager::getInstance()->get(ZipcodeTrackRepositoryInterface::class);
        parent::__construct($context, $registry);
    }

    /**
     * Save action.
     *
     * @return ResultInterface
     */
    public function execute()
    {
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            if (empty($data['id'])) {
                $data['id'] = null;
            }
            $model = $this->zipcodeTrackFactory->create();
            $zipcodeTrackId = $this->getRequest()->getParam('id');

            if ($zipcodeTrackId) {
                try {
                    $model = $this->zipcodeTrackRepository->get($zipcodeTrackId);
                } catch (LocalizedException $e) {
                    $this->messageManager->addErrorMessage(__('This Zipcode Track no longer exists.'));
                    return $resultRedirect->setPath('*/*/');
                }
            }

            if ($this->validate($data)) {
                $this->messageManager->addErrorMessage(__('Invalid zipcode track data.'));
                return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId(), '_current' => true]);
            }

            $data["zipcode_from"] = $this->helper->formatZip($data["zipcode_from"]);
            $data["zipcode_to"] = $this->helper->formatZip($data["zipcode_to"]);

            $model->setData($data);

            try {
                $this->zipcodeTrackRepository->save($model);
                $this->messageManager->addSuccessMessage(__('You saved the Zipcode Track.'));
                $this->dataPersistor->clear('avanti_redirectbygeoip_zipcodetrack');
                return $this->processZipcodeTrackReturn($model, $data, $resultRedirect);
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the Zipcode Track.'));
            }

            $this->dataPersistor->set('Avanti_RedirectByGeoip_zipcodetrack', $data);
            return $resultRedirect->setPath('*/*/edit', ['id' => $zipcodeTrackId]);
        }
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * Process and set the block return
     *
     * @param \Avanti\RedirectByGeoip\Model\ZipcodeTrack $model
     * @param array $data
     * @param ResultInterface $resultRedirect
     * @return ResultInterface
     */
    private function processZipcodeTrackReturn($model, $data, $resultRedirect): ResultInterface
    {
        $redirect = $data['back'] ?? 'close';

        if ($redirect ==='continue') {
            $resultRedirect->setPath('*/*/edit', ['id' => $model->getId()]);
        }
        if ($redirect === 'close') {
            $resultRedirect->setPath('*/*/');
        }
        return $resultRedirect;
    }

    /**
     * @param $zipcode
     * @return int
     */
    private function isValidCEP($zipcode)
    {
        try {
            return preg_match("^[0-9]{5}-[0-9]{3}$", trim($zipcode));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param $data
     * @return bool
     */
    public function validate($data) : bool
    {
        $hasError = false;
        if (!$data['zipcode_from'] && $this->isValidCEP($data['zipcode_from'])) {
            $this->messageManager->addErrorMessage(__("The field 'Zipcode From' is invalid!"));
            $hasError = true;
        }
        if (!$data['zipcode_to'] && $this->isValidCEP($data['zipcode_to'])) {
            $this->messageManager->addErrorMessage(__("The field 'Zipcode To' is invalid!"));
            $hasError = true;
        }
        return $hasError;
    }
}
