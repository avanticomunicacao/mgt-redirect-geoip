<?php

namespace Avanti\RedirectByGeoip\Controller\Adminhtml\ZipcodeTrack;

use Avanti\RedirectByGeoip\Controller\Adminhtml\ZipcodeTrack;
use Avanti\RedirectByGeoip\Api\ZipcodeTrackRepositoryInterface;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Registry;

class Delete extends ZipcodeTrack implements HttpPostActionInterface
{
    /**
     * @var ZipcodeTrackRepositoryInterface
     */
    private $zipcodeTrackRepository;

    /**
     * @param Context $context
     * @param Registry $registry
     * @param ZipcodeTrackRepositoryInterface $zipcodeTrackRepository
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ZipcodeTrackRepositoryInterface $zipcodeTrackRepository
    ) {
        $this->zipcodeTrackRepository = $zipcodeTrackRepository
            ?: ObjectManager::getInstance()->get(ZipcodeTrackRepositoryInterface::class);
        parent::__construct($context, $registry);
    }
    /**
     * Delete action.
     *
     * @return ResultInterface
     */
    public function execute()
    {
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('id');
        if ($id) {
            try {
                $this->zipcodeTrackRepository->deleteById($id);
                $this->messageManager->addSuccessMessage(__('You deleted the Zipcode Track.'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
            }
        }
        $this->messageManager->addErrorMessage(__("We can't find a Zipcode Track to delete."));
        return $resultRedirect->setPath('*/*/');
    }
}
