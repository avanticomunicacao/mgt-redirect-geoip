<?php

namespace Avanti\RedirectByGeoip\Controller\Adminhtml\ZipcodeTrack;

use Magento\Framework\Message\ManagerInterface;

class PostDataProcessor
{
    /**
     * @var ManagerInterface
     */
    protected $messageManager;

    /**
     * PostDataProcessor constructor.
     * @param ManagerInterface $messageManager
     */
    public function __construct(
        ManagerInterface $messageManager
    ) {
        $this->messageManager = $messageManager;
    }

    /**
     * @param $zipcode
     * @return int
     */
    private function isValidCEP($zipcode)
    {
        try {
            return preg_match("^[0-9]{5}-[0-9]{3}$", trim($zipcode));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param $model
     * @return bool
     */
    public function validate($model)
    {
        $hasError = false;
        if (!$model->getZipcodeFrom() && $this->isValidCEP($model->getZipcodeFrom())) {
            $this->messageManager->addErrorMessage(__("The field 'First zipcode' is invalid!"));
            $hasError = true;
        }
        if (!$model->getZipcodeTo() && $this->isValidCEP($model->getZipcodeTo())) {
            $this->messageManager->addErrorMessage(__("The field 'Last zipcode' is invalid!"));
            $hasError = true;
        }
        return $hasError;
    }
}
