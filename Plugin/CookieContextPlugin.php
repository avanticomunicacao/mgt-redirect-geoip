<?php

namespace Avanti\RedirectByGeoip\Plugin;

use Avanti\RedirectByGeoip\Api\CookieInterface;
use Avanti\RedirectByGeoip\Service\GeoIpService;
use Avanti\RedirectByGeoip\Helper\Data;
use Avanti\RedirectByGeoip\Service\ViaCepService;
use Magento\Framework\App\Http\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;
use Magento\Sniffs\Less\AvoidIdSniff;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Plugin on \Magento\Framework\App\Http\Context
 */
class CookieContextPlugin
{

    CONST CITY = 'city';
    CONST STATE = 'state';
    CONST STATE_SHORT = 'state_short';
    const ZIPCODE = 'zipcode';
    const POSTCODE = 'postcode';

    /**
     * @var GeoIpService
     */
    private $geoIpService;

    /**
     * Remote Address
     *
     * @var RemoteAddress
     */
    protected $remoteAddress;
    protected $cookieManager;
    protected $cookieInterface;
    /**
     * @var
     */
    protected $helper;

    /**
     * @var ViaCepService
     */
    private $viaCepService;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var StoreManagerInterface
     */
    private $_storeManager;

    public function __construct(
        GeoIpService $geoIpService,
        ViaCepService $viaCepService,
        Data $helper,
        CookieInterface $cookieInterface,
        RequestInterface $request,
        StoreManagerInterface $storeManager
    ) {
        $this->geoIpService = $geoIpService;
        $this->helper = $helper;
        $this->cookieInterface = $cookieInterface;
        $this->viaCepService = $viaCepService;
        $this->request = $request;
        $this->_storeManager = $storeManager;
    }

    /**
     * @param Context
     */
    public function afterGetVaryString(Context $subject)
    {
        if (!$this->helper->isUserIdentificationEnable()) {
            $this->cookieInterface->delete('city');
            $this->cookieInterface->delete('state');
            $this->cookieInterface->delete('state_short');
            $this->cookieInterface->delete('zipcode');
            return [];
        }

        if (isset($_COOKIE['city'])) {
            return [];
        }

        if (isset($_COOKIE['website_code'])) {
            return [];
        }

        $currentIp = $this->geoIpService->getClientIp();
        if ($this->helper->isDebugModeEnable()) {
            $currentIp = $this->geoIpService->validateIp($this->helper->getTestIp());
        }
        $this->setLocationData($currentIp);
        return [];
    }

    protected function setLocationData($currentIp) : void
    {
        $zipcode = $this->geoIpService->getZipcodeByIpAddress($currentIp);
        $zipcodeCookie = $this->cookieInterface->get(self::ZIPCODE);
        if (!$zipcodeCookie) {
            $this->cookieInterface->set($zipcode,self::ZIPCODE);
            $this->setLocationByGeoIP($currentIp);
        }
        $zipcodeRequest = $this->request->getParam(self::POSTCODE);
        if ($zipcodeRequest) {
            $this->cookieInterface->set($zipcodeRequest,self::ZIPCODE);
            $this->setLocationByViaCep($currentIp, $zipcodeRequest);
            return;
        }
        $this->setLocationByViaCep($currentIp);
    }

    public function setLocationByGeoIP($currentIp)
    {
        $city = $this->geoIpService->getCityByIpAddress($currentIp);
        $this->cookieInterface->set($city,'city');
        $state = $this->geoIpService->getStateByIpAddress($currentIp);
        $this->cookieInterface->set($state,'state');
        $stateShort = $this->geoIpService->getStateShortByIpAddress($currentIp);
        $this->cookieInterface->set($stateShort,'state_short');
    }

    public function setLocationByViaCep($currentIp, $zipcode = null)
    {
        if (!$zipcode) {
            $zipcode = $this->cookieInterface->get(self::ZIPCODE);
        }
        $this->viaCepService->setCep($zipcode);
        $address = json_decode($this->viaCepService->execute() ,true);
        if (!is_null($address)) {
            if (array_key_exists('erro',$address)) {
                $this->cookieInterface->set(__('Physical Store'),'city');
                $this->cookieInterface->set($this->_storeManager->getStore()->getName(),'state');
                $this->cookieInterface->set($this->_storeManager->getStore()->getName(),'state_short');            }
            if ($this->checkAddress($address)) {
                $city = $address['localidade'];
                $this->cookieInterface->set($city,'city');
                $state = $address['uf'];
                $this->cookieInterface->set($state,'state');
                $stateShort = $address['uf'];
                $this->cookieInterface->set($stateShort,'state_short');
                return;
            }
        }
        $this->setLocationByGeoIP($currentIp);
    }

    public function checkAddress($address)
    {
        if (isset($address['localidade']) && isset($address['uf'])) {
            return true;
        }
        return false;
    }

    public function clearLocationData() : void
    {
        $this->cookieInterface->delete(self::CITY);
        $this->cookieInterface->delete(self::STATE);
        $this->cookieInterface->delete(self::STATE_SHORT);
    }

    public function setZipcode($zipcode)
    {
        $this->cookieInterface->set($zipcode,self::ZIPCODE);
    }
}
